export type UserDate = {
  day: number;
  month: number;
  year: number;
};
