export enum Size {
    xxs,
    xs,
    sm,
    md,
    lg,
    xl,
    xxl
}