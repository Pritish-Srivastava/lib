export type DropdownItem = {
  label: string;
  value: string;
  icon?: string;
  active?: boolean;
  disabled?: boolean;
};
