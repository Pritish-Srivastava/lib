export enum TimeUnit {
    MINUTES,
    HOURS,
    DAYS
}