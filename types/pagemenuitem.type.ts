export type PageMenuItem = {
    path: string;
    label: string;
    icon?: string;
}

export enum PageSwitcherStyle {
    DEFAULT,
    MINIMIZED,
    THIN
}