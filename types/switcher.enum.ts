export enum SwitcherStyle {
  Default,
  Vertical,
  Horizontal,
  HorizontalAndWraps,
}

export enum SelectionItemActiveStyle {
  UNKNOWN,
  NONE,
  CIRCLE,
  SIDEBAR,
  SIDEDOT,
  BOTTOMDOT,
  ACCENT_BACKGROUND,
  ACCENT_COLOR,
}
