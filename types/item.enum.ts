export enum ItemType {
    ALL,
    UserPreferences,
    UserLocalPreferences,
    SessionStore,
    SessionStoreV2,
    Sessions,
    Task,
    Snapshot,
    Tag,
    Routine,
    CurrentTask
}