export type WindowObject = {
    documentHeight: number;
    documentWidth: number;
    landscapiness: number;
    scale: number;
    isMinimalTopBar?: boolean;
    isInPortrait: boolean;
    isInThinMode: boolean;
    firstLoad: number;
}   