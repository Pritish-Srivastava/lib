export enum DragStatus {
    NONE,
    STARTED,
    DRAGGING,
    DROPPED
}