export enum TextInputStyle {
  PLAIN,
  BOXED,
  OUTLINED,
}
